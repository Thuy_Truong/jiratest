package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateIssuePage {

	private final WebDriver driver;

	public CreateIssuePage(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return isCreateIssueDialogVisible();
            }
        });		
	}

	By createIssueDialogLocator = By.id("create-issue-dialog");
	By createLinkLocator = By.id("create_link");
	By summaryTextLocator = By.id("summary");
	By descriptionTextLocator = By.id("description");
	By createIssueSubmitLocator = By.id("create-issue-submit");
	By issueCreatedKeyLocator = By.className("issue-created-key");
	
	public boolean isCreateIssueDialogVisible()
	{
		return driver.findElement(createIssueDialogLocator).isDisplayed();
	}

	public CreateIssuePage enterSummary(String summary) {
		driver.findElement(summaryTextLocator).sendKeys(summary);
		return this;
	}

	public CreateIssuePage enterDescription(String description) {
		driver.findElement(descriptionTextLocator).sendKeys(description);
		return this;
	}

	public HomePage submitIssue() throws InterruptedException {
		driver.findElement(createIssueSubmitLocator).click();
		return new HomePage(driver);
	}

	public HomePage createIssue(String summary, String desription) throws InterruptedException {
		enterSummary(summary);
		enterDescription(desription);
		return submitIssue();
	}

	public boolean isIssueCreated(String summary) {
		return driver.findElement(issueCreatedKeyLocator).getText().contains(summary);
	}
}
