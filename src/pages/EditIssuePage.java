package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditIssuePage {
	private final WebDriver driver;

	public EditIssuePage(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return isEditIssueDialogVisible();
            }
        });
	}

	By editIssueDialogLocator = By.id("edit-issue-dialog");
	By descriptionLocator = By.id("description");
	By editIssueButtonLocator = By.id("edit-issue-submit");
	By descriptionValLocator = By.id("description-val");
	
	public boolean isEditIssueDialogVisible()
	{
		return driver.findElement(editIssueDialogLocator).isDisplayed();
	}

	public EditIssuePage updateDescription(String description) {
		WebElement descriptionInput = driver.findElement(descriptionLocator);
		descriptionInput.clear();
		descriptionInput.sendKeys(description);
		return this;
	}

	public HomePage submitEditButton() throws InterruptedException {
		driver.findElement(editIssueButtonLocator).click();
		return new HomePage(driver);
	}

	public HomePage editIssue(String desription) throws InterruptedException {
		updateDescription(desription);
		return submitEditButton();
	}

	public boolean isIssueUpdated(String desription) {
		return driver.findElement(descriptionValLocator).getText().contains(desription);
	}
}
