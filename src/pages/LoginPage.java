package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	private final WebDriver driver;

	public LoginPage(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		driver.get("https://testttbt.atlassian.net/login?dest-url=%2Fsecure%2FMyJiraHome.jspa");

		Thread.sleep(1000);
	}

	By usernameLocator = By.id("username");
	By passwordLocator = By.id("password");
	By submitbuttonLocator = By.id("login");

	public LoginPage enterUserName(String userName) {
		driver.findElement(usernameLocator).sendKeys(userName);
		return this;
	}

	public LoginPage enterPassword(String password) {
		driver.findElement(passwordLocator).sendKeys(password);
		return this;
	}

	public HomePage submitLogin() throws InterruptedException {
		driver.findElement(submitbuttonLocator).click();
		return new HomePage(driver);
	}

	public HomePage login(String userName, String password) throws InterruptedException {
		enterUserName(userName);
		enterPassword(password);
		return submitLogin();
	}
}
