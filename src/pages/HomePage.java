package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	private final WebDriver driver;

	public HomePage(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		Thread.sleep(1000);
	}

	By createLinkLocator = By.id("create_link");
	By quickSearchInputLocator = By.id("quickSearchInput");
	By editIssueLocator = By.id("edit-issue");

	public boolean isCreateLinkVisible() {
		return driver.findElement(createLinkLocator).isDisplayed();
	}

	public CreateIssuePage clickCreateLink() throws InterruptedException {
		driver.findElement(createLinkLocator).click();
		return new CreateIssuePage(driver);
	}

	public SearchResultPage searchExistingIssue(String searchName) throws InterruptedException {
		WebElement searchInput = driver.findElement(quickSearchInputLocator);
		searchInput.sendKeys(searchName);
		searchInput.submit();
		return new SearchResultPage(driver);
	}
}
