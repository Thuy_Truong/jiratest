package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {
	private final WebDriver driver;

	public SearchResultPage(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		Thread.sleep(1000);
	}

	By issueContentLocator = By.id("issue-content");
	By editIssueLocator = By.id("edit-issue");

	public boolean isResultFound(String searchName) {
		return driver.findElement(By.className("issue-link-summary")).getText().contains(searchName);
	}

	public EditIssuePage clickEditIssue() throws InterruptedException {
		driver.findElement(issueContentLocator).findElement(editIssueLocator).click();
		Thread.sleep(5000);
		return new EditIssuePage(driver);
	}

}