package test;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.CreateIssuePage;
import pages.HomePage;
import pages.LoginPage;
import pages.SearchResultPage;
import pages.EditIssuePage;

public class TestSuite {

	private static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		driver = new FirefoxDriver();

		HomePage homePage = new LoginPage(driver).login("tester", "tester123");

		testCreateIssue(homePage);
		Thread.sleep(2000);

		SearchResultPage searchResult = testSearchIssue(homePage);
		Thread.sleep(2000);

		testUpdateIssue(searchResult);
		
		driver.close();
	}

	public static void testCreateIssue(HomePage homePage) throws InterruptedException {
		System.out.println("Test create issue");

		CreateIssuePage createIssuePage = homePage.clickCreateLink();
		String summary = "TestIssue" + new Date().getTime();
		createIssuePage.createIssue(summary, "Added decription");
		Thread.sleep(2000);
		assertTrue("Failed to create a new issue", createIssuePage.isIssueCreated(summary));
	}

	public static SearchResultPage testSearchIssue(HomePage homepage) throws InterruptedException {
		System.out.println("Test search issue");

		String searchName = "existingIssue";
		SearchResultPage searchResult = homepage.searchExistingIssue(searchName);
		assertTrue("No item found", searchResult.isResultFound(searchName));
		return searchResult;
	}

	public static void testUpdateIssue(SearchResultPage searchResult) throws InterruptedException {
		System.out.println("Test update issue");

		EditIssuePage editIssuePage = searchResult.clickEditIssue();
		String description = "Update description" + new Date().getTime();
		editIssuePage.editIssue(description);
		Thread.sleep(2000);
		assertTrue("Failed to update the existingIssue", editIssuePage.isIssueUpdated(description));
	}
}
