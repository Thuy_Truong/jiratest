### What is this repository for? ###

* The repository store the project which is running the test suite of three automated browser tests for JIRA
* Link: https://bitbucket.org/Thuy_Truong/jiratest

### How do I get set up? ###

Set up

- Install Eclipse for Java 
- Use Git to get the source code
- Import the project
- Install Firefox

Configuration

- Add Firefox path to the PATH system environment

Dependencies

- Firefox
- JDK

How to run tests

- Select TestSuite.java and run

Precondition:

- The JIRA link used to test: https://testttbt.atlassian.net/login?dest-url=%2Fsecure%2FMyJiraHome.jspa (It's a trial version which is available for one week from 14-Oct-2014) 
- Test Account: 	

		Username: tester
		Password: tester123

Assumption:

- Test user logged in to JIRA successfully.
- Only an issue named "existingIssue" should exist in JIRA.
- Three test cases should be run in sequence (or at least "*testSearchIssue*" test case should be run before "*testUpdateIssue*" test case)

### Who do I talk to? ###

* Email: thuytruong1201@yahoo.com